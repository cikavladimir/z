from v1pysdk import V1Meta
import sys
import xlsxwriter
import requests
import re
import urllib3
import http.client as http
import sys, traceback
from pathlib import Path
import os
import string
import openpyxl
from openpyxl.styles import Font

http.HTTPConnection._http_vsn = 10
http.HTTPConnection._http_vsn_str = 'HTTP/1.0'

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

v1=V1Meta(instance_url = "https://adl-uat.pncint.net/VersionOne",
	  password = '1.VD6ayIKUVIh+Dt6ASABay3mMX/U=',
	  use_password_as_token=True)
resultsDE = (
	v1.Defect
	.select('Number',
                'Name',
                'Team',
                'Timebox',
                'Estimate',
                'SecurityScope',
                'CreatedBy',
                'Description',
                'Owners.Name',
                'Priority',
                'ChangeDate',
                'ChangedBy',
                'ClassOfService',
                'ClosedBy',
                'ClosedDate',
                'DoneDate',
                'Custom_DefectID',
                'Custom_DetectedByID',
                'Custom_DetectedinApplication',
                'Custom_DetectedinEnvironment2',
                'Custom_DetectedinLifecycle3',
                'FoundBy',
                'Custom_HPALMDefectStatus2',
                'Super',
                'Reference',
                'SplitFrom',
                'Status',
                'TaggedWith',
                'Custom_AutomatedControl',
                'Custom_ControlID',
                'Custom_MoSCow',
                'Custom_GroomingStatus',
                'Parent',
                'FixedInBuild',
                'Custom_DefectSeverity',
                'Dependencies.Number',
                'Dependencies.Name',
                'Dependants.Number',
                'Dependants.Name',
                'Goals.Number',
                'Goals.Name',
                'Attachments.Name',
                'Attachments.Filename',
                'Links.Name',
                'Links.URL',
                ))
	
workbook = xlsxwriter.Workbook(r'C:\Users\zx13398\Documents\ADL\ADL_Defects.xlsx', {'strings_to_urls': True})
worksheet = workbook.add_worksheet()
worksheet.set_column('A:AU', 40)
wb_format_bold = workbook.add_format({'bold': True})
wb_format_tw = workbook.add_format({'text_wrap': True})
#wb_format_bold_red = Font(bold = True,color = 'e64117')
wb_format_bold_red = ({'bold': True, 'font_color': 'red'})
bold_string = ['This has an embedded image', wb_format_bold_red ]

#need support for worksheet size limit

def write_to_excel_file(rows, rows_already_written): 
    col = 0
    for idx, row in enumerate(rows):
        #print("%s-%s" % (idx,row))
        wb_format = wb_format_bold if idx == 0 and rows_already_written == 0 else wb_format_tw
        worksheet.write_row(idx + rows_already_written, col, tuple(row), wb_format)
        #worksheet.write_rich_string(idx + rows_alreadywriten, col
            
rows = []
excel_header = (
    'Defect Number',
    'Defect Name',
    'Owner',
    'Estimate Points',
    'Planning level',
    'Created Date',
    'Created By',
    'Crew',
    'Sprint',
    'Description',
    'Priority',
    'Changed Date',
    'Changed By',
    'Class of Service',
    'Closed By',
    'Closed Date',
    'Done Date',
    #'Code Complexity Rank',
    'Status',
    'Found By',
    'Portfolio Item Number',
    'Portfolio Item Name',
    'Reference',
    'Split From ID',
    'Split From Name',
    'Tags',
    'Resolved in Build',
    'Automated Control Type',
    'Control#',
    'MoSCow',
    'Refinement Status',
    'HPALM Defect Status',
    'HPALM Defect ID',
    'Detected by ID',
    'Detected in Application',
    'Detected in Environment',
    'Detected in Lifecycle',
    'Defect Severity',
    'Upstream DependencyID',
    'Upstream Dependency Name', 
    'Downstream Dependency ID',
    'Downstream Dependency Name',
    'Business Objective Item ID',
    'Business Objective Item Name',

    'Attachments Title',
    'Attachments Filename',
    'Links Name',
    'URL',
     )
rows.append(excel_header)
row_limit = 50
rows_already_written = 0

for defect in list(resultsDE)[:10]:  
    cols = []
    cols.append(defect.Number)
    cols.append(defect.Name)
    cols.append(", ".join([o.Name for o in defect.Owners]))
    cols.append(defect.Estimate if defect.Estimate else '')
    cols.append(defect.SecurityScope.Name if defect.SecurityScope.Name else '')
    cols.append(defect.CreateDate[:-13])
    cols.append(defect.CreatedBy.Name)
    cols.append(defect.Team.Name if defect.Team.Name else '')
    cols.append(defect.Timebox.Name if defect.Timebox.Name else '')
    des = str(defect.Description)
    a1 = 'This has an embedded image' if des.find('src') > 0 else ''
    #a1 = str(bold_string) if des.find('src') > 0 else ''
    clean = re.compile('<.*?>|&nbsp;')
    cols.append(re.sub(clean, '', str(defect.Description)) + a1)  
    cols.append(defect.Priority.Name if defect.Priority.Name else '')
    cols.append(defect.ChangeDate[:-13])
    cols.append(defect.ChangedBy.Name)
    cols.append(defect.ClassOfService.Name if defect.ClassOfService.Name else '')
    cols.append(defect.ClosedBy.Name if defect.ClosedBy else '')
    cols.append(defect.DoneDate[:-13] if defect.DoneDate else '')
    cols.append(defect.ClosedDate[:-13] if defect.ClosedDate else '')
    #cols.append(defect.ComplexityRank if defect.ComplexityRank else '')
    cols.append(defect.Status.Name if defect.Status.Name else '')
    cols.append(defect.FoundBy if defect.FoundBy else '')
    cols.append(defect.Super.Number if defect.Super.Number else '')
    cols.append(defect.Super.Name if defect.Super.Name else '')
    cols.append(defect.Reference if defect.Reference else '')
    cols.append(defect.SplitFrom.Number if defect.SplitFrom.Number else '')
    cols.append(defect.SplitFrom.Name if defect.SplitFrom.Name else '')
    cols.append(defect.TaggedWith if defect.TaggedWith else '')
    cols.append(defect.FixedInBuild if defect.FixedInBuild else '')
    cols.append(defect.Custom_AutomatedControl.Name if defect.Custom_AutomatedControl.Name else '')
    cols.append(defect.Custom_ControlID if defect.Custom_ControlID else '')
    cols.append(defect.Custom_MoSCow.Name if defect.Custom_MoSCow.Name else '')
    cols.append(defect.Custom_GroomingStatus.Name if defect.Custom_GroomingStatus.Name else '')
    cols.append(defect.Custom_ALMRequirementID if defect.Custom_ALMRequirementID else '')

    cols.append(defect.Custom_DefectID if defect.Custom_DefectID else '')
    cols.append(defect.Custom_DetectedByID if defect.Custom_DetectedByID else '')
    cols.append(defect.Custom_DetectedinApplication if defect.Custom_DetectedinApplication else '')
    cols.append(", ".join([o.Name for o in defect.Custom_DetectedinEnvironment2]))
    cols.append(defect.Custom_DetectedinLifecycle3.Name if defect.Custom_DetectedinLifecycle3.Name else '')
    cols.append(defect.Custom_DefectSeverity.Name if defect.Custom_DefectSeverity.Name else '')

    dependNo = ([o.Number for o in defect.Dependencies])
    dependName = ([o.Name for o in defect.Dependencies])
    #cols.append(dependNo + "-" + dependName)
    depend = zip(dependNo, dependName)
    dependz = list(depend)
    depends = str(dependz).strip('[""''""]')
    cols.append(depends + "\n")

    dependantsNo = ([o.Number for o in defect.Dependants])
    dependantsName = ([o.Name for o in defect.Dependants])
    dependant = zip(dependantsNo, dependantsName)
    dependantz = list(dependant)
    dependants = str(dependantz).strip('[""''""]')
    cols.append(dependants + "\n")
    
    goalNo = ([o.Number for o in defect.Goals])
    goalName =([o.Name for o in defect.Goals])
    goal = zip(goalNo, goalName)
    goalz = list(goal)
    goals = str(goalz).strip('[""''""]')
    cols.append(goals + "\n")

    attachName = ([o.Name for o in defect.Attachments])
    attachFN = ([o.Filename for o in defect.Attachments])
    attach = zip(attachName, attachFN)
    attachz = list(attach)
    attaches = str(attachz).strip('[""''""]')
    cols.append(attaches + "\n") 
    
    cols.append(", ".join([o.Name for o in defect.Links]))
    cols.append(", ".join([o.URL for o in defect.Links]))
       
    
       
    rows.append(cols)
    if len(rows) >= row_limit:
        write_to_excel_file(rows, rows_already_written)
        rows_already_written += len(rows)
        rows = []
    
        
        #break  # FIXME

if rows:
    print("rcount:%s" % len(rows)) #FIXME
    write_to_excel_file(rows, rows_already_written)
    
workbook.close()   

    
