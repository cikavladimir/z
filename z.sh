Dzoni Zoom: 
https://us05web.zoom.us/j/3511812572?pwd=RitmYVZVWitMZWxVdmN6dG1HamRIQT09

Boj:
https://us04web.zoom.us/j/6395386959?pwd=NExMdEU4MS9JcVZOQ0FDbU9DN2g1dz09


https://us05web.zoom.us/j/87839097600?pwd=anpmMkgwaEVxYTNjVkNzeXpUQWUyQT09


https://meet63.webex.com/join/pr1828614231

https://meet87.webex.com/join/pr1823203060

https://meet63.webex.com/join/pr1828614231

https://databar.my.webex.com/meet/pr1828614231


6/3/2021


import re
import logging
import sqlite_utils
from v1pysdk import V1Meta

table_name = "portfolios"
primary_key = "Portfolio_ID"
db = sqlite_utils.Database(f"sqlite/version_one_pnc__{table_name}.db")

logging.basicConfig(
    filename=f'logs/pnc_{table_name}.log',
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(name)s %(message)s'
)


def get_portfolios_segment_multi_fields(range_idx, page_size):
    return (
        v1.Epic
            .select('Key',
                    'Number',  #
                    'Name',  #
                    'Category.Name',  #
                    'Swag',  #
                    'SecurityScope.Name',  #
                    'StrategicThemes',  #
                    'Goals',  #
                    'Value',  #
                    'SuperAndMe',  #
                    'Subs',  #
                    'ChangeDate',  #
                    'ChangedBy',  #
                    'ClosedBy.Name',  #
                    'DoneDate',  #
                    'CreatedBy',  #
                    'Owners',  #
                    'PlannedStart',  #
                    'PlannedEnd',  #
                    'Reference',  #
                    'Team.Name',  #
                    'Description',  #
                    'Priority.Name',  #
                    'Status.Name',  #
                    'TaggedWith',  #
                    'Dependencies',
                    'Dependants',

                    'Custom_InvestmentID',  #
                    'Custom_InvestmentName',  #
                    'Custom_RequestedBy',
                    'Custom_MoSCow2',
                    )
    ).page(start=range_idx, size=page_size)


def get_portfolios_segment_single_field(range_idx, page_size):
    return (
        v1.Epic.select('Number')
    ).page(start=range_idx, size=page_size)


def get_clean_description(p):
    try:
        description = str(p.Description)
        a1 = 'This has an embedded image' if description.find('src') > 0 else ''
        clean_description = re.compile('<.*?>|&nbsp;')
        return re.sub(clean_description, '', str(p.Description)) + a1
    except Exception as desc_error:
        print(desc_error)
        logging.error(f"Portfolio ID/Name: {p.Key} - {p.Name} description FAILED")
        return ''
        pass


def build_portfolio_row(p):
    separator = ", "
    description = get_clean_description(p)
    return {
        primary_key: p.Key,
        "Title": p.Name if p.Name else '',
        "ID": p.Number if p.Number else '',
        "Type": p.Category.Name if p.Category.Name else '',
        "Swag": p.Swag if p.Swag else '',
        "Estimate_Pts.": '',  # 'calculated field'
        "Planning_Level": p.SecurityScope.Name if p.SecurityScope.Name else '',
        "Strategic_Theme": p.StrategicThemes if p.StrategicThemes else '',
        "Business_Objectives": separator.join([o.Name for o in p.Goals]) if len(
            p.Goals) > 0 else '',
        "Business_Value_(0-100)": p.Value if p.Value else '',
        "Change_Date": p.ChangeDate if p.ChangeDate else '',
        "Changed_By": p.ChangedBy.Name if p.ChangedBy.Name else '',
        "Child_Portfolio_Items": separator.join([o.Number for o in p.SuperAndMe]),
        "Child_Backlog_Items": separator.join([o.Number for o in p.Subs]),
        "Closed_By": p.ClosedBy.Name if p.ClosedBy.Name else '',
        "Closed_Date": p.DoneDate if p.DoneDate else '',
        "Created_By": p.CreatedBy.Name if p.CreatedBy.Name else '',
        "Investment_ID": p.Custom_InvestmentID if p.Custom_InvestmentID else '',  # Custom_InvestmentID
        "Investment_Name": p.Custom_InvestmentName if p.Custom_InvestmentName else '',  # Custom_InvestmentName
        "Owners": p.Owners if p.Owners else '',
        "Planned_Begin": p.PlannedStart if p.PlannedStart else '',
        "Planned_End": p.PlannedEnd if p.PlannedEnd else '',
        "Planning_Level_Estimate": '',  #
        "Planning_Level_Path": '',  #
        "Portfolio_Item": '',  #
        "Portfolio_Item_Path": p.url if p.url else '',  #
        "Portfolio_Item_Root": '',  #
        "Reference": p.Reference if p.Reference else '',
        "Requested_By": p.Custom_RequestedBy.Name if p.Custom_RequestedBy else '',  # Custom_RequestedBy
        "Target_Crew": p.Team.Name if p.Team.Name else '',
        "Description": description,
        "Priority": p.Priority.Name if p.Priority.Name else '',
        "Status": p.Status.Name if p.Status.Name else '',
        "Tags": p.TaggedWith if p.TaggedWith else '',
        "MoSCow": p.Custom_MoSCow2.Name if p.Custom_MoSCow2 else '',
        "Upstream_Dependencies": p.Dependencies if p.Dependencies else '',
        "Downstream_Dependencies": p.Dependants if p.Dependants else '',
    }


def retry_single_field_selected(portfolios_range, size):
    portfolios_segment_single_field = get_portfolios_segment_single_field(portfolios_range, size)
    defects_selected_size_single = len(portfolios_segment_single_field)
    print(f"Portfolios selected single: {defects_selected_size_single}")

    for idx_single, portfolio in enumerate(portfolios_segment_single_field):
        print("########################")
        print(f"Single portfolio record index: {idx_single} - number: {portfolio.Number}")

        portfolio_row_single = ''
        try:
            portfolio_row_single = build_portfolio_row(portfolio)
            db[table_name].upsert_all([portfolio_row_single], pk=primary_key)
            print(f"Record stored: {portfolio.Number}")
        except Exception as single_error:
            print("##################################")
            print(f"Portfolio with single field FAILED {single_error} !!!")
            print(f"Portfolio ID: {portfolio.Key} FAILED")
            print(f"Portfolio Name: {portfolio.Name} FAILED !!!")
            print(f"Portfolio Number: {portfolio.Number} FAILED !!!")
            logging.error(f"Portfolio ID/Name: {portfolio.Key} - {portfolio.Name} FAILED")
            print("##################################")
            db[table_name].upsert_all([portfolio_row_single], pk=primary_key)
            pass
    pass


if __name__ == '__main__':
    with V1Meta(
            instance_url="https://adl-qa.pncint.net/VersionOne",
            password='1.sAxScOXv0AbavOF57NE4fiKWK84=',
            use_password_as_token=True
    ) as v1:

        portfolios = v1.Epic.select('Number')
        portfolios_count = len(portfolios)
        print("##################################")
        print(f"Total {table_name}: {portfolios_count}")
        print("##################################")

        page_start = 619
        page_size = 10
        for idx_range in range(page_start, portfolios_count, page_size):
            try:
                portfolios_segment_multi_fields = get_portfolios_segment_multi_fields(idx_range, page_size)
                portfolios_selected_size = len(portfolios_segment_multi_fields)
                print(f"Defects selected multi: {portfolios_selected_size}")

                rows = []
                for portfolio in list(portfolios_segment_multi_fields):
                    portfolio_row = build_portfolio_row(portfolio)
                    rows.append(portfolio_row)

                db[table_name].upsert_all(rows, pk=primary_key)
                print(f"Records stored: {idx_range} - {idx_range + portfolios_selected_size}")
                print("##################################")
            except Exception as multi_error:
                print(f"Defect with multi fields FAILED {multi_error} !!!")
                retry_single_field_selected(idx_range, page_size)

        print(f"FINAL - Records stored: {portfolios_count}")
